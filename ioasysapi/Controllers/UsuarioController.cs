﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ioasysapi.Models;
using ioasysapi.Models.Repository;
using ioasysapi.Service;
using Microsoft.AspNetCore.Authorization;


namespace ioasysapi.Controllers
{
    [Route("api/usuario")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IUsuarioRepository<Usuario> _usuarioRepository;

        public UsuarioController(IUsuarioRepository<Usuario> usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }

        /// <summary>
        ///   Autenticar usuario
        /// </summary>
        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Authenticate([FromBody] Usuario model)
        {
            var user = _usuarioRepository.GetLogin(model.Email, model.Senha);
            if (user == null)
                return NotFound(new { message = "Usuário não autorizado." });
            var token = TokenService.GenerateToken(user);
            user.Senha = "";
            return new
            {
                user = user,
                token = token
            };
        }

        /// <summary>
        ///   Listagem de usuarios
        /// </summary>
        [HttpGet]
        [Route("listall")]
        [Authorize(Roles = "admin, user")]
        public IActionResult Get(int? pagina)
        {
            IEnumerable<Usuario> usuarios = _usuarioRepository.GetAll(pagina);
            return Ok(usuarios);
        }

        /// <summary>
        ///   Retornar usuario
        /// </summary>
        [HttpGet("{id}", Name = "Get")]
        [Route("list")]
        [Authorize(Roles = "admin, user")]
        public IActionResult Get(long id)
        {
            Usuario usuario = _usuarioRepository.Get(id);

            if (usuario == null)
            {
                return NotFound("O Usuario não foi encontrado.");
            }

            return Ok(usuario);
        }

        /// <summary>
        ///   Cadastrar novo usuario
        /// </summary>
        [HttpPost]
        [Route("create")]
        [Authorize(Roles = "admin, user")]
        public IActionResult Post([FromBody] Usuario usuario)
        {
            if (usuario == null)
            {
                return BadRequest("Usuario is null.");
            }

            _usuarioRepository.Add(usuario);
            return CreatedAtRoute(
                  "Get",
                  new { Id = usuario.UsuarioId },
                  usuario);
        }

        /// <summary>
        ///   Editar usuario
        /// </summary>
        [HttpPut("{id}")]
        [Route("edit")]
        [Authorize(Roles = "admin, user")]
        public IActionResult Put(long id, [FromBody] Usuario usuario)
        {
            if (usuario == null)
            {
                return BadRequest("Usuario inexistente.");
            }

            Usuario usuarioToUpdate = _usuarioRepository.Get(id);
            if (usuarioToUpdate == null)
            {
                return NotFound("O Usuario não foi encontrado.");
            }

            _usuarioRepository.Update(usuarioToUpdate, usuario);
            return NoContent();
        }

        /// <summary>
        ///   Excluir usuario
        /// </summary>
        [HttpDelete("{id}")]
        [Route("remove")]
        [Authorize(Roles = "admin")]
        public IActionResult Delete(long id)
        {
            Usuario usuario = _usuarioRepository.Get(id);
            if (usuario == null)
            {
                return NotFound("O Usuario não foi encontrado.");
            }

            _usuarioRepository.Delete(usuario);
            return NoContent();
        }
    }
}
