﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ioasysapi.Models;
using ioasysapi.Models.Repository;
using ioasysapi.Service;
using Microsoft.AspNetCore.Authorization;

namespace ioasysapi.Controllers
{

    [Route("api/filme")]
    [ApiController]
    public class FilmeController : ControllerBase
    {
        private readonly Models.DataManager.IFilmeRepository<Filme> _filmeRepository;

        public FilmeController(Models.DataManager.IFilmeRepository<Filme> filmeRepository)
        {
            _filmeRepository = filmeRepository;
        }

        [HttpGet]
        [Route("listall")]
        [Authorize(Roles = "admin, user")]
        public IActionResult Get(int? pagina)
        {
            IEnumerable<Filme> filmes = _filmeRepository.GetAll(pagina);
            return Ok(filmes);
        }

       
        [HttpGet("{id}", Name = "Get")]
        [Route("list")]
        [Authorize(Roles = "admin, user")]
        public IActionResult Get(long id)
        {
            Filme filme = _filmeRepository.Get(id);

            if (filme == null)
            {
                return NotFound("O Filme não foi encontrado.");
            }

            return Ok(filme);
        }

       
        [HttpPost]
        [Route("create")]
        [Authorize(Roles = "admin, user")]
        public IActionResult Post([FromBody] Filme filme)
        {
            if (filme == null)
            {
                return BadRequest("Filme is null.");
            }

            _filmeRepository.Add(filme);
            return CreatedAtRoute(
                  "Get",
                  new { Id = filme.FilmeId },
                  filme);
        }

        
        [HttpPut("{id}")]
        [Route("edit")]
        [Authorize(Roles = "admin, user")]
        public IActionResult Put(long id, [FromBody] Filme filme)
        {
            if (filme == null)
            {
                return BadRequest("Filme inexistente.");
            }

            Filme filmeToUpdate = _filmeRepository.Get(id);
            if (filmeToUpdate == null)
            {
                return NotFound("O Filme não foi encontrado.");
            }

            _filmeRepository.Update(filmeToUpdate, filme);
            return NoContent();
        }

        
        [HttpDelete("{id}")]
        [Route("remove")]
        [Authorize(Roles = "admin")]
        public IActionResult Delete(long id)
        {
            Filme filme = _filmeRepository.Get(id);
            if (filme == null)
            {
                return NotFound("O Filme não foi encontrado.");
            }

            _filmeRepository.Delete(filme);
            return NoContent();
        }


        //public IActionResult Index()
        //{
        //    return View();
        //}
    }
}
