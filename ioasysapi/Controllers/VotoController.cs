﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ioasysapi.Models;
using ioasysapi.Models.Repository;
using ioasysapi.Service;
using Microsoft.AspNetCore.Authorization;

namespace ioasysapi.Controllers
{
    [Route("api/voto")]
    [ApiController]
    public class VotoController : ControllerBase
    {
        private readonly IVotoRepository<Voto> _votoRepository;

        public VotoController(IVotoRepository<Voto> votoRepository)
        {
            _votoRepository = votoRepository;
        }

        [HttpPost]
        [Route("create")]
        [Authorize(Roles = "user")]
        public IActionResult Post([FromBody] Voto voto)
        {
            if (voto == null)
            {
                return BadRequest("Voto is null.");
            }

            _votoRepository.Add(voto);
            return CreatedAtRoute(
                  "Get",
                  new { Id = voto.FilmeId },
                  voto);
        }
        //public IActionResult Index()
        //{
        //    return View();
        //}
    }
}
