﻿using Microsoft.EntityFrameworkCore;
using System;

namespace ioasysapi.Models
{
    public class VotoContext : DbContext
    {
        public VotoContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Voto> Votos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Voto>().HasData(new Voto
            {
                FilmeId = 1,
                UsuarioId = 1,
                Nota = 2

            }, new Voto
            {
                FilmeId = 2,
                UsuarioId = 1,
                Nota = 3
            });
        }
    }
}
