﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ioasysapi.Models
{
    public class Voto
    {
            public long FilmeId { get; set; }
            public long UsuarioId { get; set; }
            public Byte Nota { get; set; }
    }
}
