﻿using System.Collections.Generic;
using System.Linq;
using ioasysapi.Models.Repository;


namespace ioasysapi.Models.DataManager
{
    public class FilmeManager: IFilmeRepository<Filme>
    {
        readonly FilmeContext _filmeContext;

        public FilmeManager(FilmeContext context)
        {
            _filmeContext = context;
        }

        void IFilmeRepository<Filme>.Add(Filme entity)
        {
            _filmeContext.Filmes.Add(entity);
            _filmeContext.SaveChanges();
        }

        void IFilmeRepository<Filme>.AddVoto(long id, byte media)
        {
            Filme filme = _filmeContext.Filmes.FirstOrDefault(e => e.FilmeId == id);
            filme.MediaVoto = media;
            _filmeContext.SaveChanges();
        }

        void IFilmeRepository<Filme>.Delete(Filme filme)
        {         
            filme.Ativo = 0;
            _filmeContext.SaveChanges();
        }

        Filme IFilmeRepository<Filme>.Get(long id)
        {
            return _filmeContext.Filmes.FirstOrDefault(e => e.FilmeId == id);
        }

        IEnumerable<Filme> IFilmeRepository<Filme>.GetAll(int? pagina)
        {
            int tamanhopagina = 10;
            int numeropagina = pagina ?? 1;
            return _filmeContext.Filmes.OrderBy(m => m.Nome).Skip((numeropagina - 1) * tamanhopagina).Take(tamanhopagina).ToList();
        }
      

        void IFilmeRepository<Filme>.Update(Filme filme, Filme entity)
        {
            filme.Nome = entity.Nome;
            _filmeContext.SaveChanges();
        }
       
    }
}
