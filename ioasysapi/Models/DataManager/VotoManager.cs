﻿using System;
using System.Collections.Generic;
using System.Linq;
using ioasysapi.Models.Repository;

namespace ioasysapi.Models.DataManager
{
    public class VotoManager : IVotoRepository<Voto>
    {
        readonly VotoContext _votoContext;
        public VotoManager(VotoContext context)
        {
            _votoContext = context;
        }
        public long Add (Voto entity)
        {
            _votoContext.Votos.Add(entity);
            _votoContext.SaveChanges();

            //comtablizando o total de votos
            long id = entity.FilmeId;            
            List<Voto> lvotos = _votoContext.Votos.Where(e => e.FilmeId == id).ToList();
            long countLVotos = lvotos.Count();
            long sumVotos = 0;
            lvotos.ForEach(x => { sumVotos += x.Nota; });

            return (sumVotos / countLVotos);

        }
    }
}
