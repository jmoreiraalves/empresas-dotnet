﻿using System.Collections.Generic;
using System.Linq;
using ioasysapi.Models.Repository;

namespace ioasysapi.Models.DataManager
{
    public class UsuarioManager : IUsuarioRepository<Usuario>
    {
        readonly UsuarioContext _usuarioContext;
        public UsuarioManager(UsuarioContext context)
        {
            _usuarioContext = context;
        }

        public IEnumerable<Usuario> GetAll(int? pagina)
        {
            int tamanhopagina = 10;
            int numeropagina = pagina ?? 1;
            return _usuarioContext.Usuarios.OrderBy(m => m.Nome).Skip((numeropagina-1) * tamanhopagina).Take(tamanhopagina).ToList();
        }

        public Usuario Get(long id)
        {
            return _usuarioContext.Usuarios.FirstOrDefault(e => e.UsuarioId == id);
        }        

        public void Add(Usuario entity)
        {
            _usuarioContext.Usuarios.Add(entity);
            _usuarioContext.SaveChanges();
        }

        public void Update(Usuario usuario, Usuario entity)
        {
            usuario.Nome        = entity.Nome;
            usuario.Email       = entity.Email;
            usuario.Senha       = entity.Senha;
            usuario.Cadastro    = entity.Cadastro;
            usuario.Telefone    = entity.Telefone;
            usuario.Aniversario = entity.Aniversario;
            usuario.Role        = entity.Role;
            usuario.Ativo       = entity.Ativo;

            _usuarioContext.SaveChanges();
        }

        public void Delete(Usuario usuario)
        {            
            usuario.Ativo = 0;

            _usuarioContext.SaveChanges();

            //_usuarioContext.Usuarios.Remove(usuario);
            //_usuarioContext.SaveChanges();
        }

        public Usuario GetLogin(string email, string senha)
        {
            return _usuarioContext.Usuarios.FirstOrDefault(e => e.Email.ToLower() == email.ToLower() & e.Senha.ToLower() == senha.ToLower());
        }
    }
}
