﻿using Microsoft.EntityFrameworkCore;
using System;

namespace ioasysapi.Models
{
    public class FilmeContext : DbContext
    {
        public FilmeContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Filme> Filmes { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            _ = modelBuilder.Entity<Filme>().HasData(new Filme

            {
                FilmeId = 1,
                Nome = "Filme Uncle",
                Cadastro = new DateTime(),
                MediaVoto = 0

            }, new Filme
            {
                FilmeId = 2,
                Nome = "Filme 2",
                Cadastro = new DateTime(),
                MediaVoto = 0
            }); ; ;
        }
    }
}
