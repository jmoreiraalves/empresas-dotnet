﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ioasysapi.Models.Repository
{
    public interface IVotoRepository<TEntity>
    {
        long Add(TEntity entity);
    }
}
