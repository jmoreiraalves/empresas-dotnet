﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ioasysapi.Models.DataManager
{
    public interface IFilmeRepository<TEntity>
    {
        IEnumerable<TEntity> GetAll(int? pagina);
        TEntity Get(long id);
        void Add(TEntity entity);
        void AddVoto(long id, byte media);
        void Update(Filme filme, TEntity entity);
        void Delete(Filme filme);
    }
}
