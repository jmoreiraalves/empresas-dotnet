﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ioasysapi.Models.Repository
{
    public interface IUsuarioRepository<TEntity>
    {
        IEnumerable<TEntity> GetAll(int? pagina);
        TEntity Get(long id);
        TEntity GetLogin(string email, string senha);
        void Add(TEntity entity);
        void Update(Usuario usuario, TEntity entity);
        void Delete(Usuario usuario);
    }
}
