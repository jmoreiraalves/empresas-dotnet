﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ioasysapi.Models
{
    public class Usuario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long UsuarioId { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public DateTime Cadastro { get; set; }
        public DateTime Aniversario { get; set; }
        public string Telefone { get; set; }
        public string Role { get; set; }
        public byte Ativo { get; set; }

    }
}
