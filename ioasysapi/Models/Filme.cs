﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ioasysapi.Models
{
    public class Filme
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long FilmeId { get; set; }
        public string Nome { get; set; }
        public byte MediaVoto { get; set; }
        public DateTime Cadastro { get; set; }
        public byte Ativo { get; set; }
        //public virtual ICollection<Voto> Votos { get; set; }
    }

}
