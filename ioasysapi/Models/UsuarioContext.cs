﻿using Microsoft.EntityFrameworkCore;
using System;

namespace ioasysapi.Models
{
    public class UsuarioContext : DbContext
    {
        public UsuarioContext(DbContextOptions options)
               : base(options)
        {
        }

        public DbSet<Usuario> Usuarios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usuario>().HasData(new Usuario
            {
                UsuarioId   = 1,
                Nome        = "João Carlos",
                Email       = "jmoreiraalves@gmail.com",
                Senha       = "jc123",
                Cadastro    = new DateTime(),
                Aniversario = new DateTime(1979, 04, 25),
                Telefone    = "999-888-7777",
                Role        = "User",
                Ativo       = 1

            }, new Usuario
            {
                UsuarioId   = 2,
                Senha       = "Aline",
                Nome        = "al123",
                Email       = "aline@gmail.com",
                Cadastro    = new DateTime(),
                Aniversario = new DateTime(1981, 07, 13),
                Telefone    = "111-222-3333",
                Role        = "Admin",
                Ativo       = 1
            }); ; ;
        }
    }
}
